#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyRT11.
#
#  pyRT11 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyRT11 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyRT11.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""This module provides classes and functions to support manipulating images of
RT-11 filesystems as used on DEC PDP-11 computers.

Prefix blocks are not yet supported.
Extra bytes in directory entries are untested, and may or may not work."""

from rt11 import rt11util, rad50

import os
import math
from collections import namedtuple


# Directory entry parameters in 16-bit words (not bytes)
rtDirEntSize    = 7     # Dir entry size, not including extra padding                 
rtDirSegHdrSize = 5     # Dir segment header size
rtDirSegSize    = 512   # Dir segment size (2x 512-byte block = 512 words)


# How many boot blocks? Normally blocks 0, 2, 3, 4, 5
rtNumBootBlocks = 5

# Directory entry status word flags
deETENT = 0o000400 # Tentative file
deEMPTY = 0o001000 # Empty area
deEPERM = 0o002000 # Permanent file
deEEOS  = 0o004000 # End of segment marker
deEREAD = 0o040000 # Read-only
deEPROT = 0o100000 # Protected
deEPRE  = 0o000020 # Prefix block(s) present


def statWordToStr(statWord, omit_perm_flag = False):
    """Generate string representing directory entry status word."""
    statStr = ""
    if statWord & deETENT:
        statStr += "E.TENT "
    if statWord & deEMPTY:
        statStr += "E.EMPTY "
    if (statWord & deEPERM) and not omit_perm_flag:
        statStr += "E.PERM "
    if statWord & deEEOS:
        statStr += "E.EOS "
    if statWord & deEREAD:
        statStr += "E.READ "
    if statWord & deEPROT:
        statStr += "E.PROT "
    if statWord & deEPRE:
        statStr += "E.PRE "
    return statStr.rstrip()


# Define known disk format types
# Some of these parameters are a bit questionable...
fsType = namedtuple('fsType', 'desc blocks dirsegs reserved')
fsTypeList = {
    'max4':    fsType('Maximum volume size, 4 dir entries',  65535,  4,  0),
    'max31':   fsType('Maximum volume size, 31 dir entries', 65535, 31,  0),
    'rl01':    fsType('RL01 disk pack',                      10225, 16, 53),
    'rl02':    fsType('RL02 disk pack',                      20465, 31, 83),
    'rx02':    fsType('RX02 8" floppy',                        988,  4, 14),
    'tu58':    fsType('TU58 DECtape-II',                       512,  4,  0)
}


class dirEntry:
    """Class representing an RT-11 directory entry and its associated data."""

    deStatus  = 0                         # Status word
    deName    = [0, 0, 0]                 # Filename + extension, Radix-50
    deLength  = 0                         # File length (blocks)
    deStart   = 0                         # First data block number
    deJobChan = 0                         # Job/Channel number (tentative files only)
    deDate    = 0                         # Creation date word
    deExtra   = []                        # Optional extra directory entry padding (words)
    dePrefix  = []                        # Optional prefix blocks
    deData    = []                        # Data blocks

    def __init__(self,
                 name,
                 length,
                 start,
                 status  = deEPERM,
                 jobchan = 0,
                 date    = None,
                 extra   = []):

        """dirEntry class constructor.

        Does not allocate data blocks. Use fsetData() or deEmptyData()
        to initialize data blocks.
        If date is negative, today's date will be substituted."""

        self.deStatus  = status
        self.deName    = name
        self.deLength  = length
        self.deStart   = start
        self.deJobChan = jobchan
        self.deExtra   = extra
        if len(self.deName) > 3:
            self.deName = self.deName[0:3]
        if date is not None:
            self.deDate    = date
        else:
            date = rt11util.todayWord()


            
    def deSetData(self, data):
        """Set contents of data blocks.

        Length of data must be deLength * rt11util.rtBlockSize."""

        rt11util.assertError(len(data) == (self.deLength * rt11util.rtBlockSize),
                             "deSetData() received invalid data length.")
        self.deData = data
        

    def deEmptyData(self):
        """Initialize file with empty data blocks."""
        
        self.deData = b'\000' * (rt11util.rtBlockSize * self.deLength)

        
    def dePrintName(self):
        """Print the filename."""
        print(rad50.rad50ToFilename(self.deName))
        
        
    def dePrintEntryInfo(self):
        """Print information about this directory entry."""

        print(rad50.rad50ToFilename(self.deName) + ":")
        print("  status  = " + statWordToStr(self.deStatus))
        print("  length  = " + str(self.deLength))
        print("  start   = " + str(self.deStart))
        print("  date    = " + rt11util.dateWordToStr(self.deDate))
        print("  jobchan = " + str(self.deJobChan))
        print("  extra   = " + str(self.deExtra))


    def dePrintDirHeader(self, long_format=False):
        """Print header lines for sunsequent dePrintDirEntry() calls."""

        dirline = 'Name            Size   Date'
        if long_format:
            dirline = dirline + '          Start  Flags'
            print(dirline)
            print('-'*64)
        else:
            print(dirline)
            print('-'*34)


    def dePrintDirEntry(self, long_format=False, omit_perm_flag=True):
        """Print directory listing line for this directory entry."""

        dirline = rad50.rad50ToFilename(self.deName).ljust(12) \
          + str(self.deLength).rjust(8) \
          + rt11util.dateWordToStr(self.deDate).rjust(14)

        if long_format:
            dirline = dirline + str(self.deStart).rjust(8) \
              + "  " + statWordToStr(self.deStatus, omit_perm_flag) \

        print(dirline)

    def deSaveFile(self, hostFilename=""):
        """Save to a file on the host computer filesystem.

        hostFilename argument specifies the filename to create on the
        host filesystem.
        If name for host file is not specified, RT-11 filename will
        be used, and the file will be saved in the current directory.
        If host filename is specified, it may include a relative
        or absolute path in order to save the file outside the current
        directory."""

        if hostFilename == "":
            hostFilename = rad50.rad50ToFilename(self.deName)

        f = open(hostFilename, "wb")
        f.write(self.deData)
        f.close

        

class fileSystem:
    """Class representing an RT-11 filesystem."""

    fsSize          = -1                # Filesystem size in blocks
    fsPackClustSize = 1                 # Pack cluster size (?)
    fsFirstDirSeg   = 6                 # Block num of first directory segment
    fsSysVersion    = 0                 # System version (Radix-50)
    fsVolumeId      = ""                # Volume ID (ASCII, 12 chars)
    fsOwnerName     = ""                # Owner name (ASCII, 12 chars)
    fsSystemId      = ""                # System ID (ASCII, 12 chars)
    fsNumDirSegs    = -1                # Number of directory segments (1-31)
    fsExtraWords    = 0                 # Extra words per directory entry
    fsDirEntSize    = 0                 # Dir entry size (words)
    fsDirEntPerSeg  = 0                 # Maximum dir entries per segment
    fsDataStart     = 0                 # First data block
    fsResBlocks     = 0                 # Reserved blocks at end of filesystem
    fsDataBlocks    = 0                 # Number of data blocks for files
    fsVerbose       = False             # Enable verbose debugging messages
    
    fsBootBlocks    = []

    fsFileList      = []                # List of dirEntry objects

    fsThisYear      = None              # Pretend it is this year if not None
    
    def __init__(self,
                 blocks     = 512,
                 dirSegs    = 1,
                 extraWords = 0,
                 resBlocks  = 0,
                 sysVersion = b'V3A',
                 volId      = b'RT11A',
                 ownerName  = b'',
                 sysId      = b'DECRT11A',
                 bootBlocks = [b'\000'*rt11util.rtBlockSize]*rtNumBootBlocks,
                 thisYear   = None):
        """fileSystem class constructor."""
        

        self.fsSize          = blocks
        self.fsResBlocks     = resBlocks
        self.fsNumDirSegs    = dirSegs
        self.fsExtraWords    = extraWords
        self.fsDirEntSize    = rtDirEntSize + self.fsExtraWords
        self.fsDirEntPerSeg  = (rtDirSegSize - rtDirSegHdrSize) // self.fsDirEntSize
        self.fsSysVersion    = rad50.ascToRad50(sysVersion)[0]
        self.fsVolumeId      = volId.ljust(12)
        self.fsOwnerName     = ownerName.ljust(12)
        self.fsSystemId      = sysId.ljust(12)
        self.fsDataStart     = self.fsFirstDirSeg + (self.fsNumDirSegs * 2)
        self.fsBootBlocks    = bootBlocks
        self.fsThisYear      = thisYear

        self.fsDataBlocks = self.fsSize - self.fsDataStart - self.fsResBlocks
        
        if len(self.fsVolumeId) > 12:
            self.fsVolumeId = self.fsVolumeId[0:12]

        if len(self.fsOwnerName) > 12:
            self.fsOwnerName = self.fsOwnerName[0:12]

        if len(self.fsSystemId) > 12:
            self.fsSystemId = self.fsSystemId[0:12]

        dirEnt = dirEntry(name   = rad50.filenameToRad50(" EMPTY.FIL"),
                        length = self.fsSize - (self.fsDataStart + self.fsResBlocks),
                        start  = self.fsDataStart,
                        status = deEMPTY,
                        date   = rt11util.todayWord(self.fsThisYear))
        dirEnt.deEmptyData()
        
        self.fsFileList = [dirEnt]

        self.fsValidate()


    def fsValidate(self):
        """Sanity-check filesystem parameters."""
        rt11util.assertError(self.fsSize > 9, "Filesystem is too small.") # ?
        rt11util.assertError(self.fsSize < 65536, "Filesystem is too large.")
        rt11util.assertError(self.fsNumDirSegs > 0, "No directory segments.")
        rt11util.assertError(self.fsNumDirSegs < 32, "Too many directory segments.")
        rt11util.assertError(self.fsExtraWords >= 0, "Negative extra dir entry byte count.")
        rt11util.assertWarn(self.fsFirstDirSeg == 6, "Unexpected first directory segment block number.")
        rt11util.assertWarn(self.fsDataStart == (self.fsFirstDirSeg + (self.fsNumDirSegs * 2)),
                            "Data blocks do not start immediately after directory segments.")
        rt11util.assertError(self.fsDataStart + self.fsDataBlocks + self.fsResBlocks == self.fsSize,
                             "Invalid block counts.")
        rt11util.assertError(self.fsResBlocks >= 0, "Negative reserved block count.")


    def _fsReadHomeBlock(self, f):
        """Parse the home block (block 1)"""
        if self.fsVerbose:
            print("_fsReadHomeBlock():")
        blk = rt11util.readBlocks(f, 1, 1)
        # TO DO: Block replacement table, etc.
        self.fsPackClustSize = rt11util.rtWord(blk, 0o722)
        self.fsFirstDirSeg   = rt11util.rtWord(blk, 0o724)
        self.fsSysVersion    = rt11util.rtWord(blk, 0o726)
        self.fsVolumeId      = blk[0o730:0o744]
        self.fsOwnerName     = blk[0o744:0o760]
        self.fsSystemId      = blk[0o760:0o774]
        self.fsFileList      = []
        self.fsDataBlocks    = 0
        if (self.fsFirstDirSeg == 0x0000) or (self.fsFirstDirSeg == 0x2020):
            # Have seen VAX-11/730 console tapes with spaces or zeros in place of
            # fsPackClustSize, fsFirstDirSeg and fsSysVersion.
            # So if first dir segment is 0x2020, assume this is such an image
            # and assume a hopefully sane default.
            self.fsFirstDirSeg   = 6
            if self.fsVerbose:
                print("    Overriding bogus fsFirstDirSeg value")
        if self.fsVerbose:
            print("    fsPackClustSize =", self.fsPackClustSize)
            print("    fsFirstDirSeg   =", self.fsFirstDirSeg)
            print("    fsSysVersion    =", self.fsSysVersion)
            print("    fsVolumeId      =", self.fsVolumeId)
            print("    fsOwnerName     =", self.fsOwnerName)
            print("    fsSystemId      =", self.fsSystemId)


    def _fsReadBootBlocks(self, f):
        """Load the boot blocks"""
        self.fsBootBlocks    = []
        for n in [0] + list(range(2, rtNumBootBlocks + 1)):
            self.fsBootBlocks.append(rt11util.readBlocks(f, n, 1))

    def _fsReadDirSegs(self, f):
        """Parse the directory segments.

        Directory segments are 2 blocks long, start at block number
        fsFirstDirSeg, and are numbered 1 through 31.
        We ignore garbage in unused directory segments."""

        if self.fsVerbose:
            print("_fsReadDirSegs():")
            
        dsNum = 1
        while dsNum > 0:
            # Read directory segment, convert to 16-bit words
            bnum = self.fsFirstDirSeg + ((dsNum - 1)*2)
            if self.fsVerbose:
                print("    blocks {:d},{:d}".format(bnum, bnum+1))
            blk  = rt11util.readBlocks(f, bnum, 2)
            wblk = rt11util.bytesToWords(blk)

            # Extract fields from directory segment header
            numDs, nextDs, highDs, xBytes, dStart = wblk[:rtDirSegHdrSize]

            if self.fsVerbose:
                print("        numDs  =", numDs)
                print("        nextDs =", nextDs)
                print("        highDs =", highDs)
                print("        xBytes =", xBytes)
                print("        dStart =", dStart)

            rt11util.assertError((xBytes % 2) == 0, "Extra bytes count must be even.")
            
            if dsNum == 1:
                # Initialize stuff from first dir segment header
                self.fsNumDirSegs    = numDs
                self.fsExtraWords    = xBytes // 2
                self.fsDirEntSize    = rtDirEntSize + self.fsExtraWords
                self.fsDirEntPerSeg  = (rtDirSegSize - rtDirSegHdrSize) // self.fsDirEntSize
                self.fsDataStart     = dStart
            else:
                # Sanity checks
                # Each segment should have matching number of dir segs count in header
                rt11util.assertWarn(numDs == self.fsNumDirSegs,
                                    "Number of directory segments changed in segment " + str(dsNum))

                # Each segment should have matching extra byte count in header
                rt11util.assertWarn(xBytes // 2 == self.fsExtraWords,
                                    "Number of extra entry bytes changed in segment " + str(dsNum))

                # Dir segments should point to contiguous sets of data blocks
                rt11util.assertWarn(dStart == dsDataBlock,
                                    "Non-contiguous data in directory segment " + str(dsNum))

            # Unpack remainder of directory segment into list of
            # directory entry tuples.
            # This line is so ugly, it ought to be in Perl.
            # Don't ask me how it works; I barely understood it when I wrote it.
            dsEntries = zip(*[iter(wblk[rtDirSegHdrSize:])]*self.fsDirEntSize)
            
            dsDataBlock = dStart

            for dsEntry in dsEntries:
                statWord, fn1, fn2, ext, fileLen, jobChan, fileDate = dsEntry[:7]

                if (statWord & deEEOS):
                    # End of this directory segment
                    break

                entry = dirEntry(name = [fn1, fn2, ext],
                                 length = fileLen,
                                 start  = dsDataBlock,
                                 status = statWord,
                                 jobchan = jobChan,
                                 date    = fileDate,
                                 extra   = rt11util.wordsToBytes(dsEntry[7:]))

                self.fsFileList.append(entry)
                self.fsDataBlocks += fileLen
                dsDataBlock = dsDataBlock + fileLen

            if (nextDs > 0):
                rt11util.assertWarn(nextDs == dsNum + 1, "Non-sequential directory segments.")
            dsNum = nextDs


    def _fsReadDataBlocks(self, f):
        """Read the data blocks from the filesystem."""

        for entry in self.fsFileList:
            entry.deSetData(rt11util.readBlocks(f, entry.deStart, entry.deLength))


    def fsRead(self, imgfile):
        """Read RT-11 filesystem from file named (imgfile)."""

        f = open(imgfile, "rb")

        # Get size of file, in blocks
        f.seek(0, os.SEEK_END)
        self.fsSize = f.tell() // rt11util.rtBlockSize
        if self.fsVerbose:
            print("fsRead({:s}) read {:d} blocks".format(imgfile, self.fsSize))

        self._fsReadHomeBlock(f)
        self._fsReadBootBlocks(f)
        self._fsReadDirSegs(f)
        self._fsReadDataBlocks(f)
        
        f.close()

        self.fsResBlocks = self.fsSize - (self.fsDataStart + self.fsDataBlocks)
        self.fsValidate()


    def fsSqueeze(self):
        """Squeeze the filesystem.

        Removes tentative files and consolidates unallocated blocks
        in single empty file at end of filesystem."""

        oldFileList = self.fsFileList
        self.fsFileList = []
        dataBlock = self.fsDataStart
        
        for de in oldFileList:
            if de.deStatus & deEPERM:
                de.deStart = dataBlock
                dataBlock += de.deLength
                self.fsFileList.append(de)

        remainingBlocks = (self.fsDataStart + self.fsDataBlocks) - dataBlock

        if remainingBlocks > 0:
            eb = dirEntry(name   = rad50.filenameToRad50(" EMPTY.FIL"),
                          length = remainingBlocks,
                          start  = dataBlock,
                          status = deEMPTY,
                          date   = rt11util.todayWord(self.fsThisYear))
            eb.deEmptyData()
            self.fsFileList.append(eb)
            
        self.fsValidate()
        

    def fsPrintDirNames(self):
        """Print directory listing, names only."""
        for e in self.fsFileList:
            if e.deStatus & deEPERM:
                e.dePrintName()


        
    def fsPrintDir(self, longFormat = False):
        """Print directory listing."""

        permBlocks  = 0
        tentBlocks  = 0
        emptyBlocks = 0
        emptyFrags  = 0
        
        e = self.fsFileList[0]
        e.dePrintDirHeader(longFormat)

        for e in self.fsFileList:
            if (e.deStatus & deEPERM) or longFormat:

                if e.deStatus & deEPERM:
                    permBlocks += e.deLength

                if e.deStatus & deETENT:
                    tentBlocks += e.deLength

                if e.deStatus & deEMPTY:
                    emptyBlocks += e.deLength
                    emptyFrags += 1
                    
                e.dePrintDirEntry(longFormat)
                
        if longFormat:
            print(" ")
            print(str(self.fsSize).rjust(5), "total blocks")
            print(str(self.fsDataBlocks).rjust(5), "data blocks")
            print(str(self.fsSize - self.fsDataBlocks).rjust(5), "reserved blocks")
            if permBlocks > 0:
                print(str(permBlocks).rjust(5), "blocks used by permanent files")
            if tentBlocks > 0:
                print(str(tentBlocks).rjust(5), "blocks used by tentative files")
            if emptyBlocks > 0:
                print(str(emptyBlocks).rjust(5), "blocks free in", emptyFrags, "fragments")
            print(str(self.fsNumDirSegs).rjust(5), "directory segments")
            print(str(len(self.fsFileList)).rjust(5), "of",
                self.fsDirEntPerSeg * self.fsNumDirSegs, "directory entries used")


    def _fsWriteBootBlocks(self, f):
        for n in range(rtNumBootBlocks):
            b = n
            if n > 0:
                b = b + 1
            rt11util.writeBlocks(f, self.fsBootBlocks[n], b, 1)


    def _fsWriteHomeBlock(self, f):
        """Write the home block.

        A lot of the reserved fields of the block are not yet supported."""

        blk = bytearray(rt11util.rtBlockSize)

        blk[0o722:0o724] = rt11util.rtBytes(self.fsPackClustSize)
        blk[0o724:0o726] = rt11util.rtBytes(self.fsFirstDirSeg)
        blk[0o726:0o730] = rt11util.rtBytes(self.fsSysVersion)
        blk[0o730:0o744] = self.fsVolumeId
        blk[0o744:0o760] = self.fsOwnerName
        blk[0o760:0o774] = self.fsSystemId

        rt11util.writeBlocks(f, blk, 1, 1)


    def _fsWriteDirSegs(self, f):
        """Write the directory segments."""

        # How many of the dir segments will we use? Need to
        # hold files in file list plus end of segment marker per dir seg
        dsNeeded = math.ceil((len(self.fsFileList) + self.fsNumDirSegs) // self.fsDirEntPerSeg)
        
        # Sanity check: Will the entries fit?
        rt11util.assertError(dsNeeded <= self.fsNumDirSegs,
                             'Not enough directory segments to hold file list.')
        
        # Start constructing dir segs in memory. Represent as lists of 16-bit
        # word values for now.
        dirSegs = [None]*self.fsNumDirSegs
        for dsNum in range(self.fsNumDirSegs):
            dirSegs[dsNum] = [0]*rt11util.rtBlockSize

        # Fill in header info that we already know
        for dsNum in range(len(dirSegs)):
            dirSegs[dsNum][:rtDirSegHdrSize] = [self.fsNumDirSegs, 0, 0, self.fsExtraWords*2, 0]

        # Fill in file information

        dsNum  = 1
        deNum  = 0
        highDs = 0
        blkNum = self.fsDataStart
        
        for de in self.fsFileList:

            # Fill in more directory entry header information
            if deNum == 0:
                dirSegs[dsNum-1][1] = dsNum + 1
                dirSegs[dsNum-1][4] = blkNum


            # Fill in one directory entry
            deOffset = rtDirSegHdrSize + (deNum * self.fsDirEntSize)

            dirSegs[dsNum-1][deOffset:deOffset+rtDirEntSize] = [de.deStatus,
                                                              de.deName[0],
                                                              de.deName[1],
                                                              de.deName[2],
                                                              de.deLength,
                                                              de.deJobChan,
                                                              de.deDate]


            rt11util.assertError(blkNum == de.deStart, 'Block number calculation error.')

            blkNum = blkNum + de.deLength

            if self.fsDirEntSize > rtDirEntSize:
                # Fill in extra bytes
                dirsegs[dsNum-1][deOffset+rtDirEntSize:deOffset+self.fsDirEntSize] = de.deExtra

            deNum = deNum + 1
            if deNum == (self.fsDirEntPerSeg - 1):
                # Add end of segment marker
                deOffset = rtDirSegHdrSize + (deNum * self.fsDirEntSize)
                dirSegs[dsNum-1][deOffset] = deEEOS
                deNum = 0
                dsNum = dsNum + 1

        if deNum != 0:
            # If deNum == 0, we should have just written an end of segment
            # marker. If not, add one now.
            deOffset = rtDirSegHdrSize + (deNum * self.fsDirEntSize)
            dirSegs[dsNum-1][deOffset] = deEEOS

        # Next dir seg pointer = 0
        dirSegs[dsNum-1][1] = 0


        # Now plug in high dir seg number
        highDs = dsNum
        for dsNum in range(len(dirSegs)):
            dirSegs[dsNum][2] = highDs

        # Write the constructed dir segs to disk
        blks = bytearray(0)
        for ds in dirSegs:
            blks += rt11util.wordsToBytes(ds)
        rt11util.writeBlocks(f, blks, self.fsFirstDirSeg, self.fsNumDirSegs * 2)


    def _fsWriteDataBlocks(self, f):
        """Write the data blocks."""
        for entry in self.fsFileList:
            rt11util.writeBlocks(f, entry.deData, entry.deStart, entry.deLength)


    def fsWrite(self, imgfile):
        """Write RT-11 filesystem to file named (imgfile)."""

        f = open(imgfile, "wb")

        # Expand file to full size
        f.seek((self.fsSize * rt11util.rtBlockSize) - 1, os.SEEK_SET)
        f.write(b'\000')
        f.seek(0, os.SEEK_SET)

        # Write stuff
        self._fsWriteBootBlocks(f)
        self._fsWriteHomeBlock(f)
        self._fsWriteDirSegs(f)
        self._fsWriteDataBlocks(f)

        f.close()



    def _fsConsolidateEmpty(self):
        """Consolidate adjacent empty files."""
        
        oldFileList = self.fsFileList[1:]
        self.fsFileList = self.fsFileList[:1]

        for de in oldFileList:
            if de.deStatus & deEMPTY:
                if self.fsFileList[-1].deStatus & deEMPTY:
                    # End of list is also empty, so combine them
                    de.deLength += self.fsFileList[-1].deLength
                    de.deName = self.fsFileList[-1].deName
                    de.deData = self.fsFileList[-1].deData + de.deData
                    self.fsFileList[-1] = de
                else:
                    # End of list is not empty, so add this entry to end
                    self.fsFileList.append(de)
            else:
                # Entry is not empty, so add to list
                self.fsFileList.append(de)

                

    def fsDeleteFile(self, filename):
        """Delete file with specified filename.

        First permanent file with matching name will be deleted.
        Not an error if file is not found.
        Adjacent empty files will be consolidated after deletion."""

        fnRad50 = rad50.filenameToRad50(filename)
        
        for idx, de in enumerate(self.fsFileList):
            if fnRad50 == de.deName:
                de.deStatus = deEMPTY
                self.fsFileList[idx] = de

        self._fsConsolidateEmpty()


    def fsSaveFile(self,
                   rtFilename,
                   hostFilename = None):
        """Save permanent file with specified name to host filesystem.

        If hostFilename is not specified, RT-11 filename will be used
        and file will be saved in current directory. If host filename
        is specified, it may include a relative or absolute path."""

        r50Name = rad50.filenameToRad50(rtFilename)
        if hostFilename is None:
            hfn = rad50.rad50ToFilename(r50Name)
        else:
            hfn - hostFilename

        savedFile = False
        for de in self.fsFileList:
            if (de.deStatus & deEPERM) and (de.deName == r50Name):
                savedFile = True
                de.deSaveFile(hfn)
                break
        rt11util.assertWarn(savedFile, "Could not find file " + rtFilename)
        

    def fsSaveAll(self):
        """Save all permanent files to host filesystem."""

        for de in self.fsFileList:
            if (de.deStatus & deEPERM):
                de.deSaveFile()


    def fsAddFile(self,
                  hostFilename,
                  rtFilename   = "",
                  dateWord     = None,
                  statusWord   = deEPERM):
        """Read file and add it to the RT-11 filesystem.

        hostFilename refers to a local file on the host's filesystem.
        If rtFilename is not specified, filename on RT-11 filesystem will
        be derived from hostFilename.
        If host file size is not a multiple of rtBlockSize, it will be
        padded with zeroes.
        Negative dateWord will be replaced with today's date."""

        # Use host filename if RT-11 name not specified
        if rtFilename == "":
            rtFilename = hostFilename

        rad50Filename = rad50.filenameToRad50(rtFilename)
        rtFilename    = rad50.rad50ToFilename(rad50Filename)
        
        if dateWord is None:
            dateWord = rt11util.todayWord(self.fsThisYear)

        # Read host file, pad if necessary
        f = open(hostFilename, "rb")
        buf = f.read()
        f.close
        pad = rt11util.rtBlockSize - (len(buf) % rt11util.rtBlockSize)
        if pad != 512:
            buf += b'\000'*pad
        fileLen = len(buf)//rt11util.rtBlockSize
            
        # Delete existing file with same name
        self.fsDeleteFile(rtFilename)

        # Search for first empty block large enough to hold file
        addedFile = False
        for idx, de in enumerate(self.fsFileList):
            if (de.deStatus & deEMPTY) and (de.deLength >= fileLen):
                addedFile = True
                
                # If empty block is larger than new file, split it.
                if de.deLength > fileLen:
                    ede = dirEntry(name   = rad50.filenameToRad50(" EMPTY.FIL"),
                                   length = de.deLength - fileLen,
                                   start  = de.deStart + fileLen,
                                   status = deEMPTY,
                                   date   = rt11util.todayWord(self.fsThisYear))
                    ede.deEmptyData()
                    self.fsFileList.insert(idx+1, ede)
                
                de.deStatus  = statusWord
                de.deName    = rad50Filename
                de.deLength  = fileLen
                de.deJobChan = 0
                de.deDate    = dateWord
                de.deSetData(buf)
        
                self.fsFileList[idx] = de
                break

        rt11util.assertError(addedFile, "Could not find space for " + rtFilename)
                


    def fsPrintFsInfo(self):
        """Print information about filesystem."""

        print('fsSize          = ' + str(self.fsSize))
        print('fsPackClustSize = ' + str(self.fsPackClustSize))
        print('fsFirstDirSeg   = ' + str(self.fsFirstDirSeg))
        print('fsNumDirSegs    = ' + str(self.fsNumDirSegs))
        print('fsExtraBytes    = ' + str(self.fsExtraBytes))
        print('fsDataStart     = ' + str(self.fsDataStart))
        print('fsDataBlocks    = ' + str(self.fsDataBlocks))
        print('fsResBlocks     = ' + str(self.fsResBlocks))
        print('fsSysVersion    = "' + rad50.rad50ValToAsc(self.fsSysVersion) + '"')
        print('fsVolumeId      = "' + self.fsVolumeId + '"')
        print('fsOwnerName     = "' + self.fsOwnerName + '"')
        print('fsSystemId      = "' + self.fsSystemId + '"')
        print(" ")
        for e in self.fsFileList:
            e.dePrintEntryInfo()

        
    def fsSaveBootBlocks(self, hostFilename):
        """Save the boot blocks to the specified file."""

        f = open(hostFilename, "wb")
        rt11util.writeBlockList(f, self.fsBootBlocks)
        f.close()


    def fsLoadBootBlocks(self, hostFilename):
        """Load the boot blocks from the specified file."""

        f = open(hostFilename, "rb")
        for n in range(rtNumBootBlocks):
            self.fsBootBlocks[n] = rt11util.readBlocks(f)
        f.close()

    def fsSetYear(self, year):
        """Set the year to use for today's date."""
        self.fsThisYear = year
