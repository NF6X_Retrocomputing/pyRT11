#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyRT11.
#
#  pyRT11 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyRT11 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyRT11.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""This package provides support for manipulating images of DEC RT-11 filesystems."""

__all__ = ['rt11util', 'rad50', 'rt11fs']
__version__ = '0.3.0'

