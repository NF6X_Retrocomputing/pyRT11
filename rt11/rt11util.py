#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyRT11.
#
#  pyRT11 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyRT11 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyRT11.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""This module provides assorted utility functions used by the rest of this package."""

import os
import sys
import datetime

# Size of an RT-11 filesystem block in bytes
rtBlockSize = 512

# Enable flags for assertion wrappers
enableWarnings = True                     # enable assertWarn()
enableErrors   = True                     # enable assertError()
demoteErrors   = False                    # demote errors to warnings


rtMonthList = ["",
               "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
               "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

    
def dateWordToStr(dateWord):
    """Return ASCII string representation of date word (dateWord).

    Supports Age bits for dates through year 2100."""

    if dateWord == 0:
        return "           "

    ageNum   = ((dateWord >> 14) & 0o03)
    monthNum = ((dateWord >> 10) & 0o17)
    dayNum   = ((dateWord >>  5) & 0o37)
    yearNum  = (dateWord         & 0o37)

    assertError(monthNum > 0,  "Month number out of valid range (too small).")
    assertError(monthNum < 13, "Month number out of valid range (too large).")
    assertError(dayNum   > 0,  "Day number out of valid range (too small).")
    assertError(dayNum   < 32, "Day number out of valid range (too large).")

    year = yearNum + 1972 + (ageNum * 32)

    dateStr = str(dayNum).zfill(2)
    dateStr = dateStr + '-' + rtMonthList[monthNum]
    dateStr = dateStr + '-' + str(year)

    return dateStr
        
def todayWord(year=None):
    """Return date word representing today's date in the local timezone.

    Uses nonzero age field, which may not be supported by all RT-11 tools.
    Fails at year 2100. Remind me to fix it in mid-2099.

    If year argument is set, pretend it is the specified year."""

    todayDate = datetime.date.today()
    if year is not None:
        theYear = year
    else:
        theYear = todayDate.year
    assertError(theYear >= 1972, 'Years prior to 1972 are not supported.')
    assertError(theYear <= 2100, 'Years beyond 2099 are not supported.')
    ageNum   = ((theYear - 1972) >> 5) & 0o03
    yearNum  = (theYear - 1972) & 0o37
    monthNum = todayDate.month & 0o17
    dayNum   = todayDate.day & 0o37

    return (ageNum << 14) | (monthNum << 10) | (dayNum << 5) | yearNum


def rtBytes(wordVal):
    """Returns 2-byte bytearray with provided word value in little-endian order."""

    b = bytearray(2)
    b[0] = wordVal & 0o377
    b[1] = (wordVal >> 8) & 0o377
    return b


def rtWord(buf, offset = 0):
    """Returns word value at byte offset (offset) of buffer (buf).

    offset must be even.
    Not sensitive to endianness of host system."""

    assertError((offset % 2) == 0, "rtWord() offset must be even.")
    assertError(len(buf) > (offset + 1), "rtWord() offset past end of buffer.")
    return buf[offset] | (buf[offset + 1] << 8)


def writeBlockList(file, blockList):
    """Writes list of blocks to open file at current file position.

    blockList shall be a list of blocks, each rtBlockSize in length."""

    assertError(len(blockList) > 0, 'Empty block list passed to writeBlocks().')
    for block in blockList:
        assertError(len(block) == rtBlockSize, 'Bad block length')
        file.write(block)
    

def writeBlocks(file, buffer, num = -1, count = 1):
    """Write (count) blocks starting at (num) to open file object (file).

    Block count defaults to 1 (512 bytes).
    If num is negative, then write starting at current file offset; otherwise
    seek to beginning of specified block before writing.
    File offset is left immediately after last byte written.
    When num is negative, current file offset is assumed to be a multiple
    of block size."""
    
    assertError(len(buffer) == rtBlockSize * count,
                  'rt11util.writeBlocks(): Buffer length does not match block count')
    if (num >= 0):
        file.seek(num * rtBlockSize, os.SEEK_SET)
    file.write(buffer)


def readBlocks(file, num = -1, count = 1):
    """Reads (count) blocks starting at (num) from open file object (file).

    Block count defaults to 1 (512 bytes).
    If num is negative, then read starting at current file offset; otherwise
    seek to beginning of specified block before reading.
    File offset is left immediately after last byte read.
    When num is negative, current file offset is assumed to be a multiple
    of block size."""

    if (num >= 0):
        file.seek(num * rtBlockSize, os.SEEK_SET)
    buffer = file.read(rtBlockSize * count)
    assertError(len(buffer) == rtBlockSize * count,
                  'rt11util.readBlocks(): Buffer length does not match block count.')
    return buffer

def bytesToWords(buf):
    """Convert buffer of bytes to list of 16-bit word values.

    readBlocks() returns a list of byte values. If an occasional pair of bytes
    needs to be interpreted as a 16-bit PDP-11 word, then the rtWord() function
    suffices. If a lot of bytes need to be interpreted as 16-bit values, then
    this function can be used to conver the entire buffer to a list of 16-bit
    values. This function is insensitive to the host machine's endianness.
    If buf length is odd, last byte will be ignored."""

    wordList = []
    for lo, hi in zip(*[iter(buf)]*2):
        wordList.append(lo | (hi << 8))
    return wordList
    
def wordsToBytes(buf):
    """Convert list of 16-bit word values to little-endian array of bytes.

    Opposite of bytesToWords() function."""

    byteList = bytearray(0)
    for w in buf:
        byteList.append(w & 0o377)
        byteList.append((w >> 8) & 0o377)
    return byteList


def assertWarn(assertion, message):
    """Verify that assertion is true. If not, issue warning message to stderr."""

    if enableWarnings:
        if not assertion:
            print("WARNING:", message, file=sys.stderr)

        
def assertError(assertion, message):
    """Verify that assertion is true. If not, issue error message to stderr and exit.

    If global demoteErrors is set, pretend failure is just a warning.
    This is sometimes helpful for debugging."""

    if enableErrors:
        if not assertion:
            if demoteErrors:
                print("WARNING:", message, file=sys.stderr)
            else:
                raise RuntimeError(message)
                #print("ERROR:", message, file=sys.stderr)
                #exit(1)

            

        
