#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyRT11.
#
#  pyRT11 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyRT11 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyRT11.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Installation script for pyRT11.

pyRT11 provides support for manipulating images of DEC RT-11 filesystems.

Usage examples:

  ./setup.py install --user
  sudo ./setup.py install
  sudo ./setup.py install --prefix /usr/local
"""


from distutils.core import setup
from rt11 import __version__

setup(name          = 'pyRT11',
      version       = __version__,
      description   = 'RT-11 Filesystem Manipulation Tools',
      author        = 'Mark J. Blair',
      author_email  = 'nf6x@nf6x.net',
      url           = 'http://www.nf6x.net/tags/pyrt11/',
      download_url  = 'https://github.com/NF6X/pyRT11',
      license       = 'GPLv3',
      packages      = ['rt11'],
      scripts       = ['rt11fstool.py'])

