# pyRT11: Python Support for Manipulating DEC RT-11 Filesystems

Copyright (C) 2014 by Mark J. Blair <nf6x@nf6x.net>

pyRT11 is a Python-based package and utility for manipulating DEC RT-11 filesystem images. It can read an RT-11 filesystem into memory, manipulate the in-memory representation, then write the in-memory representation out to a new (or the same) image file. The in-memory representation presently does not handle bad block tables, block replacement information, etc., so some information is lost when manipulating many images.

The rt11fstool.py utility uses command-line arguments in an unconventional manner. Each argument is executed on the in-memory filesystem representation in the order it is encountered, having cumulative effect. If the last argument isn't a `-write` command, then the in-memory filesystem representation is discarded on exit.

pyRT11 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.