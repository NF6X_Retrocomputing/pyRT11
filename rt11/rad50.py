#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyRT11.
#
#  pyRT11 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyRT11 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyRT11.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""This module provides support routines for working with DEC Radix-50 encoding
as used by RT-11 on PDP-11 computers."""

from rt11 import rt11util

import os

# List of the ASCII characters represented by the 40 (50 octal) Radix-50 symbols.
rad50CharSet = [' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
                  'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
                  'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                  'X', 'Y', 'Z', '$', '.', '%', '0', '1',
                  '2', '3', '4', '5', '6', '7', '8', '9']


def ascToRad50(inStr):
    """Translate ASCII string (inStr) to list of Radix-50 encoded words.

    Each three characters of inStr are converted to a 16-bit Radix-50 word.
    If inStr does not contain a multiple of 3 bytes, then the final Radix-50
    word will be effectively padded with spaces.
    inStr is case-insensitive.
    Unsupported characters in inStr will be converted to spaces."""

    ascList = inStr.upper()
    rad50list = []
    while len(ascList) > 0:
        c = ascList[0]
        ascList = ascList[1:]
        try:
            rad50val = rad50CharSet.index(c)
        except:
            # replace unsupported character with space
            rad50val = 0
        rad50word = rad50val * 1600
        if len(ascList) > 0:
            c = ascList[0]
            ascList = ascList[1:]
            try:
                rad50val = rad50CharSet.index(c)
            except:
                # replace unsupported character with space
                rad50val = 0
            rad50word = rad50word + rad50val * 40
        if len(ascList) > 0:
            c = ascList[0]
            ascList = ascList[1:]
            try:
                rad50val = rad50CharSet.index(c)
            except:
                # replace unsupported character with space
                rad50val = 0
            rad50word = rad50word + rad50val
        rad50list.append(rad50word)
    return rad50list


def rad50ValToAsc(rad50word):
    """Translate single Radix-50 code (rad50word) to an ASCII string."""

    ascStr = ""
    rad50val = rad50word // 1600
    rad50word = rad50word - (rad50val * 1600)
    if rad50val > 39:
        # replace invalid most significant char with space
        rad50val = 0
    ascStr = ascStr + rad50CharSet[rad50val]
    rad50val = rad50word // 40
    rad50word = rad50word - (rad50val * 40)
    ascStr = ascStr + rad50CharSet[rad50val]
    ascStr = ascStr + rad50CharSet[rad50word]
    return ascStr


def rad50ListToAsc(inRad50):
    """Translate list of Radix-50 codes (inRad50) to an ASCII string."""

    ascStr = ""
    for rad50word in inRad50:
        ascStr = ascStr + rad50ValToAsc(rad50word)
    return ascStr


def filenameToRad50(inStr):
    '''Returns a 3-word list representing the supplied filename string (inStr) in Radix-50.

    inStr is assumed to be of the form "FILNAM.EXT" where FILNAM is the filename,
    and EXT is the extension.
    Lower case characters will be converted to upper case.
    Unsupported characters will be converted to spaces. If no period is present, then
    extension will be converted to three spaces.
    If a leading path is present, it will be stripped.'''

    bName = os.path.basename(inStr)
    fName, fExt = os.path.splitext(bName)
    fName += "      "
    fExt  = fExt[1:] + "   "
    if len(fName) > 6:
        fName = fName[0:6]
    if len(fExt) > 3:
        fExt = fExt[0:3]
    rad50list = ascToRad50(fName)
    rad50list.append(ascToRad50(fExt)[0])
    return rad50list


def rad50ToFilename(inRad50):
    """Returns ASCII string of filename represented by list of Radix-50 words (inRad50).

    inRad50 must be exactly 3 words long, with the first two words representing the
    filename and the final word representing the extension. Filename and extension
    will be trimmed of trailing spaces."""

    rt11util.assertError(len(inRad50) == 3, "rad50ToFilename() requires 3-word list.")
    fname = rad50ListToAsc(inRad50[0:2]).rstrip(' ')
    fext  = rad50ValToAsc(inRad50[2]).rstrip(' ')
    return fname + '.' + fext
