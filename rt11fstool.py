#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyRT11
#
#  pyRT11 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyRT11 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with py  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""RT-11 filesystem manipulation tool"""

import argparse
import textwrap
from rt11 import *
from rt11 import __version__


# Accumulate arguments in order specified
class gatherArgs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not 'argSequence' in namespace:
            setattr(namespace, 'argSequence', [])
        prev = namespace.argSequence
        prev.append((self.dest, values))
        setattr(namespace, 'argSequence', prev)


# Main entry point when called as an executable script.
if __name__ == '__main__':
    fs = rt11fs.fileSystem()

    parser = argparse.ArgumentParser(
        prog='rt11fstool.py',
        description=textwrap.dedent("""\
            RT-11 filesystem manipulation tool version {:s}

            Arguments are processed in the order encountered, with cumulative
            effects upon the filesystem image buffer. The filesystem image
            buffer is discarded at program exit if not saved by a final
            --write argument.
            """.format(__version__)),
        epilog=textwrap.dedent("""\
        Example:
            rt11fstool.py --read disk.img --dir"""),
        add_help=True,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('--verbose', '-v', action=gatherArgs, nargs=0,
                        help='Turn on verbose messages during processing.')

    parser.add_argument('--init', '-i', action=gatherArgs, nargs=1,
                        metavar='DiskType',
                        help="""Initialize the RT-11 filesystem, formatted
                        for the specified disk type.""")

    parser.add_argument('--list', '-l', action=gatherArgs, nargs=0,
                        help='List supported disk types for -init command.')

    parser.add_argument('--read', '-r', action=gatherArgs, nargs=1,
                        metavar='localfile',
                        help='Read RT-11 filesystem image file into memory.')

    parser.add_argument('--write', '-w', action=gatherArgs, nargs=1,
                        metavar='localfile',
                        help='Write RT-11 filesystem to image file on disk.')

    parser.add_argument('--dir', '-d', action=gatherArgs, nargs=0,
                        help="""Print RT-11 directory listing. Regular format,
                        omitting non-permanent files and usage statistics.""")

    parser.add_argument('--long', '-L', action=gatherArgs, nargs=0,
                        help="""Print RT-11 directory listing. Long format,
                        including non-permanent files, usage statistics
                        and format parameters.""")

    parser.add_argument('--names', '-n', action=gatherArgs, nargs=0,
                        help="""Print RT-11 directory listing. File names only,
                        omitting non-permanent files.""")

    parser.add_argument('--squeeze', '-S', action=gatherArgs, nargs=0,
                        help='Squeeze (defragment) the RT-11 filesystem.')

    parser.add_argument('--add', '-a', action=gatherArgs, nargs='+',
                        metavar='localfile',
                        help="""Add file(s) to the RT-11 filesystem.
                        Existing files with same names will be overwritten.""")

    parser.add_argument('--save', '-s', action=gatherArgs, nargs='+',
                        metavar='RT11file',
                        help="""Save file(s) in RT-11 filesystem to disk.
                        Local files with same name will be overwritten.""")

    #parser.add_argument('--tsave', '-t', action=gatherArgs, nargs='+',
    #                    metavar='RT11file',
    #                    help="""Save file(s) in RT-11 filesystem to disk
    #                    as text files. Nulls and ^Z will be omitted.
    #                    Local files with same name will be overwritten.""")

    parser.add_argument('--bsave', '-b', action=gatherArgs, nargs=1,
                        metavar='localfile',
                        help="""Save RT-11 filesystem boot blocks to
                        specified file. 512 * 5 = 2560 bytes will
                        be saved.""")

    parser.add_argument('--bload', '-B', action=gatherArgs, nargs=1,
                        metavar='localfile',
                        help="""Load RT-11 filesystem boot blocks from
                        specified file. Only first 512 * 5 = 2560 bytes
                        will be read. If file is too short, it will be
                        padded with zero bytes. """)

    parser.add_argument('--extract', '-e', action=gatherArgs, nargs=0,
                        help="""Save all permanent files in RT-11 filesystem to
                        disk. Local files with same name will be overwritten.""")

    parser.add_argument('--delete', '-D', action=gatherArgs, nargs='+',
                        metavar='RT11file',
                        help='Delete file(s) in RT-11 filesystem.')

    parser.add_argument('--year', '-y', action=gatherArgs, nargs=1,
                        metavar='year',
                        help="Set year to use for newly created files.")

    parser.add_argument('--validate', '-V', action=gatherArgs, nargs=0,
                        help='Validate the RT-11 filesystem.')


    args = parser.parse_args()

    fs = rt11fs.fileSystem()

    
    if not 'argSequence' in args:
        setattr(args, 'argSequence', [])

    for arg in args.argSequence:
        cmd = arg[0]
        opt = arg[1]

        if   cmd == 'init':
            if opt[0] in rt11fs.fsTypeList:
                spec = rt11fs.fsTypeList[opt[0]]
                fs = rt11fs.fileSystem(blocks    = spec.blocks,
                                       dirSegs   = spec.dirsegs,
                                       resBlocks = spec.reserved)
            else:
                rt11util.assertError(False, 'Unknown filesystem type.' \
                                     ' Use --list option for list of valid types.')

        elif   cmd == 'list':
            print('Type     Description')
            print('------------------------------------------------')
            for fs in sorted(rt11fs.fsTypeList):
                print('{:8s} {:s}'.format(fs, str(rt11fs.fsTypeList[fs].desc)))
            
        elif cmd == 'read':
            fs.fsRead(opt[0])
            
        elif cmd == 'write':
            fs.fsWrite(opt[0])
            
        elif cmd == 'dir':
            fs.fsPrintDir(False)
            
        elif cmd == 'long':
            fs.fsPrintDir(True)
            print(' ')
            
        elif cmd == 'names':
            fs.fsPrintDirNames()
            
        elif cmd == 'squeeze':
            fs.fsSqueeze()
            
        elif cmd == 'add':
            for fileName in opt:
                fs.fsAddFile(fileName)
                
        elif cmd == 'save':
            for fileName in opt:
                fs.fsSaveFile(fileName)
            
        #elif cmd == 'tsave':
        #    print(cmd, opt)
        #    print('FEATURE NOT YET IMPLEMENTED.')
            
        elif cmd == 'bsave':
            fs.fsSaveBootBlocks(opt[0])
            
        elif cmd == 'bload':
            fs.fsLoadBootBlocks(opt[0])
            
        elif cmd == 'extract':
            fs.fsSaveAll()
            
        elif cmd == 'delete':
            for fileName in opt:
                fs.fsDeleteFile(fileName)

        elif cmd == 'validate':
            fs.fsValidate()

        elif cmd == 'year':
            fs.fsSetYear(int(opt[0]))
            
        elif cmd == 'verbose':
            fs.fsVerbose = True
            
        else:
            rt11util.assertError(False, 'Internal parser error: Unexpected command "' + cmd + '"')
